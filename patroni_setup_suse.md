Hi all, in this article I guide you through how to setup PostgreSQL 16 on OpenSUSE 15.5 with patroni.

First of all we need to understand some concept. Patroni is a wrapper on top of regular PostgreSQL replication,
providing automatic failover in case master crashes.

We'll use Qemu/KVM for virtualisation.

Create a base image:
```qemu-img create -f qcow2 patroni-base.qcow2 100G```

We're going to use this a base image if later we screw up something.

So open up Virtual Machine Manager and create a VM

Create a new virtual machine use the previously created patroni-base.qcow2.
When installation is done, switch off the VM.

Now copy the base image 3 times:
```sh
cp patroni-base.qcow2 patroni-1.qcow2
cp patroni-base.qcow2 patroni-2.qcow2
cp patroni-base.qcow2 patroni-3.qcow2
```




We start with etcd since patroni depends on it:

etcdctl endpoint status --cluster -w table





Create a data directory where we are going to store our postgres instance data. It can be anywhere
and since we want to run patroni as ```postgres``` user we chown it.
````
mkdir -p /data/my-first-cluster
chown postgres:postgres /data/my-first-cluster
chmod 700 /data/my-first-cluster

Probably the easiest way to have a patroni config is to ask it generate config for you, let's do that.
```patroni --generate-sample-config```

Now, we need to tailor it for our needs 



When done, check if didn't make a mistake.
```patroni --validate-config```


To make sure it work let's start the cluster by hand for now. Very soon we'll going to create a systemd
configuration to manage startup and shutdown patroni.
```sudo -u postgres patroni cluster_conf.yml```


Automate the patroni cluster startup by utilizin systemd. Create a file in ```/etc/systemd/system/``` 
I'll name it ```patroni.service``` with the following content
```
[Unit]
Description=Runners to orchestrate a high-availability PostgreSQL
After=syslog.target network.target

[Service]
Type=simple

User=postgres
Group=postgres

ExecStart=/usr/bin/patroni /data/meh.yml
KillMode=process
TimeoutSec=30
Restart=no

[Install]
WantedBy=multi-user.target
```

save it and enable/start it

```systemctl enable --now patroni.service```

# HA Proxy

Patroni Rest API support differentaion of the modes eg: whether the server runs as primary or as a replica.
https://patroni.readthedocs.io/en/latest/rest_api.html


primary: Patroni node is running as the primary with leader lock:
replica: Patroni node is in the state running, the role is replica and noloadbalance tag is not set.

Benefit of such setup is to be able to distribute read only statements among the followers

```
defaults
  mode tcp  

listen stats
  bind 0.0.0.0:7000
  bind :::80 v6only
  stats enable
  stats uri     /
  stats refresh 5s
  # For HAProxy admin interface the http mode is needed otherwise the status page won't load
  mode http

listen postres_primary
  bind *:5000
  option httpchk OPTIONS /primary
  http-check expect status 200
  default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions  
  # It's important to keep in mind ```host:port``` combination have to be provided here. 
  server patroni-1 192.168.122.48:5432 maxconn 100 check port 8008
  server patroni-2 192.168.122.116:5432 maxconn 100 check port 8008
  server patroni-3 192.168.122.45:5432 maxconn 100 check port 8008
  
listen postgres_replica
  bind *:5001
  option httpchk OPTIONS /replica
  http-check expect status 200
  default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions  
  # It's important to keep in mind ```host:port``` combination have to be provided here. The port should be matched with
  # what is defined in the patorni postgres config. Even if you leave it on the default postgres port it has to be
  # defined here
  server patroni-1 192.168.122.48:5432 maxconn 100 check port 8008
  server patroni-2 192.168.122.116:5432 maxconn 100 check port 8008
  server patroni-3 192.168.122.45:5432 maxconn 100 check port 8008
```

If you see such error:

```psql: error: connection to server at "192.168.122.32", port 5000 failed: received invalid response to SSL negotiation: H```
Then check Ha Proxy mode it should be set to ```tcp``` and not to ```http```

mode can be set in the defaults block and individually in each listen block


